package hu.referenciaim.oahh.validation;

import hu.referenciaim.oahh.domain.dto.AccountRegistrationCommand;
import hu.referenciaim.oahh.service.AccountService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccountRegistrationCommandValidator implements Validator {

    private AccountService accountService;

    private HttpServletRequest request;

    public AccountRegistrationCommandValidator(AccountService accountService, HttpServletRequest request) {
        this.accountService = accountService;
        this.request = request;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return AccountRegistrationCommand.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (accountService.findByIpAddress(request.getRemoteAddr()) != null) {
            errors.rejectValue("userName", "ipAddress.already.used");
        }
    }
}
