package hu.referenciaim.oahh.controller;

import hu.referenciaim.oahh.domain.Account;
import hu.referenciaim.oahh.domain.Gender;
import hu.referenciaim.oahh.domain.dto.AccountDetails;
import hu.referenciaim.oahh.domain.dto.AccountRegistrationCommand;
import hu.referenciaim.oahh.security.AuthenticatedAccountDetails;
import hu.referenciaim.oahh.service.AccountService;
import hu.referenciaim.oahh.service.EmailServiceImpl;
import hu.referenciaim.oahh.validation.AccountRegistrationCommandValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;
    private EmailServiceImpl emailService;
    private AccountRegistrationCommandValidator accountRegistrationCommandValidator;

    @Autowired
    public AccountController(AccountService accountService, EmailServiceImpl emailService, AccountRegistrationCommandValidator accountRegistrationCommandValidator) {
        this.accountService = accountService;
        this.emailService = emailService;
        this.accountRegistrationCommandValidator = accountRegistrationCommandValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(accountRegistrationCommandValidator);
    }


    @GetMapping
    public ResponseEntity<List<AccountDetails>> getAllAccounts() {
        List<AccountDetails> accountDetails = new ArrayList<>();
        List<Account> accounts = accountService.findAll();
        for (Account account : accounts) {
            accountDetails.add(new AccountDetails(account));
        }
        Collections.sort(accountDetails);
        return new ResponseEntity<>(accountDetails, HttpStatus.OK);
    }

    @GetMapping("/myAccountDetails")
    public ResponseEntity<AccountDetails> getMyAccountDetails(Principal principal) {

        String userName = principal.getName();
        Account myAccount = accountService.findMyAccount(userName);
        if (myAccount != null) {
            AccountDetails myAccountDetails = new AccountDetails(myAccount);

            return new ResponseEntity<>(myAccountDetails, HttpStatus.OK);
        } else {
            logger.debug("Account not found for userName: " + userName);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/me")
    public ResponseEntity<AuthenticatedAccountDetails> getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user = (UserDetails) authentication.getPrincipal();
        System.out.println("felhasználónév: " + user.getUsername());

        return new ResponseEntity<>(new AuthenticatedAccountDetails(user), HttpStatus.OK);
    }

    @GetMapping("/gender")
    public ResponseEntity<List<Gender>> getAllCurrencies() {
        List<Gender> genderList = Arrays.asList(Gender.values());
        return new ResponseEntity<>(genderList, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> registerNewAccount(@RequestBody @Valid AccountRegistrationCommand accountRegistrationCommand) throws ParseException {
        accountService.transformPasswordToHashed(accountRegistrationCommand);
        accountService.create(new Account(accountRegistrationCommand));

        String text = "Kedves " + accountRegistrationCommand.getUserName() + " !" + System.lineSeparator()
                + "Köszönjük, hogy regisztrált honlapunkon!" + System.lineSeparator() +
                "Regisztrációs adataid:" + System.lineSeparator() + System.lineSeparator() +
                "Email címed: " + accountRegistrationCommand.getEmail() + System.lineSeparator() +
                "Felhasználóneved: " + accountRegistrationCommand.getUserName() + System.lineSeparator() +
                "Születésnapod: " + accountRegistrationCommand.getBirthDay() + System.lineSeparator() +
                "Nemed: " + accountRegistrationCommand.getGender() + System.lineSeparator() +
                "Ez egy automatikus email, kérlej erre ne válaszolj";
        emailService.sendSimpleMessage(accountRegistrationCommand.getEmail(), "Üdvözöllek az Opel Astra H Hungary weboldalon!SecurityConfig", text);


        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}