package hu.referenciaim.oahh.service;

import hu.referenciaim.oahh.domain.Account;
import hu.referenciaim.oahh.domain.dto.AccountDetails;
import hu.referenciaim.oahh.domain.dto.AccountRegistrationCommand;
import hu.referenciaim.oahh.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AccountService {

    private PasswordEncoder passwordEncoder;
    private AccountRepository accountRepository;

    @Autowired
    public AccountService(PasswordEncoder passwordEncoder, AccountRepository accountRepository) {
        this.passwordEncoder = passwordEncoder;
        this.accountRepository = accountRepository;
    }

    public Account create(Account account) {
        accountRepository.save(account);
        return account;
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account findMyAccount(String userName) {
        return findByUserName(userName);
    }

    public Account findByUserName(String userName) {
        return accountRepository.findByUserName(userName);
    }

    public List<AccountDetails> getAllAccountDetailsExceptOwn(Account myAccount) {
        List<AccountDetails> accountDetails = new ArrayList<>();
        List<Account> accounts = accountRepository.findAll();
        accounts.remove(myAccount);
        for (Account account : accounts) {
            accountDetails.add(new AccountDetails(account));
        }
        return accountDetails;
    }

    public void transformPasswordToHashed(AccountRegistrationCommand accountRegistrationCommand) {
        String transformedPassword = passwordEncoder.encode(accountRegistrationCommand.getPassword());
        accountRegistrationCommand.setPassword(transformedPassword);
    }

    public Account findById(Long id) {
        return accountRepository.findById(id).get();
    }

    public Account findByIpAddress(String ipAddress) {
        return accountRepository.findByIpAddress(ipAddress);
    }
}
