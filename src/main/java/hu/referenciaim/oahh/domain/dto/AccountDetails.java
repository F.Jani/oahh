package hu.referenciaim.oahh.domain.dto;

import hu.referenciaim.oahh.domain.Account;

public class AccountDetails implements Comparable<AccountDetails> {

    private Long id;
    private String userName;
    private String birthDay;
    private String gender;
    private String email;

    public AccountDetails(Account account) {
        this.id = account.getId();
        this.userName = account.getUserName();
        this.birthDay = account.getBirthDay().toString().substring(0, 10);
        this.gender = account.getGender().toString();
        this.email = account.getEmail();
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public String getSGender() {
        return gender;
    }

    @Override
    public int compareTo(AccountDetails otherAccountDetails) {
        return otherAccountDetails.userName.compareTo(this.userName);
    }
}
