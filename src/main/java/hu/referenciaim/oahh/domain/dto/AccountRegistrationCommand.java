package hu.referenciaim.oahh.domain.dto;

import hu.referenciaim.oahh.domain.Gender;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AccountRegistrationCommand {

    @NotNull
    @Size(min = 4, max = 30, message = "{userName.boundaries}")
    private String userName;

    private String birthDay;

    @NotNull
    @Size(min = 5, max = 30, message = "Password must be between 5-30 characters")
    private String password;

    @NotNull
    @Size(min = 5, max = 50, message = "Email must be between 5-50 characters")
    private String email;

    @NotNull
    private Gender gender;

    public AccountRegistrationCommand() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
