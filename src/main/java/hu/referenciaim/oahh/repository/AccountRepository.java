package hu.referenciaim.oahh.repository;

import hu.referenciaim.oahh.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByUserName(String userName);

    Account findById(String id);

    Account findByIpAddress(String ipAddress);
}
